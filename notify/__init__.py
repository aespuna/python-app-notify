import json
from datetime import datetime
from string import hexdigits

import logging
import requests
from hyper import HTTP20Connection
from hyper.tls import init_context


_logger = logging.getLogger(__name__)


def is_ios(push_id):
    #
    # iOS tokens match a hex string (while android ones won't)
    # This is faster than a compiled regex (^[0-9a-fA-F]+$)
    return all(char in hexdigits for char in push_id)


def android_notifier(api_key, user_agent='python-app-notify/1.0', encoder_cls=json.JSONEncoder):
    def send_to_gcm(push_id, payload):
        resp = requests.post(
            'https://gcm-http.googleapis.com/gcm/send',
            headers={
                'User-Agent': user_agent,
                'Authorization': 'key=' + api_key,
                'Content-Type': 'application/json',
            },
            data=json.dumps({'registration_ids': [push_id], 'data': payload}, cls=encoder_cls).encode('utf8'),
        )
        json_resp = resp.json()
        return json_resp['success'] == 1, json_resp

    return send_to_gcm


def _add(dictionary, name, field):
    if field:
        dictionary[name] = field


def make_ios_payload(alert=None, badge=None, sound=None, category=None, custom=None, content_available=False):
    notification = {}

    _add(notification, 'alert', alert)
    _add(notification, 'badge', badge)
    _add(notification, 'sound', sound)
    _add(notification, 'category', category)
    _add(notification, 'content-available', content_available)

    result = dict(aps=notification)
    if custom is not None:
        result.update(custom)

    return result


def ios_notifier(cert_file, key_file=None, password=None, topic=None, sandbox=False, alternative_port=False,
                 encoder_cls=json.JSONEncoder):
    port = 2197 if alternative_port else 443
    ssl_context = init_context()
    try:
        ssl_context.load_cert_chain(cert_file, key_file, password=password)
    except FileNotFoundError:
        print('[WARNING] iOS notifier will not work because invalid cert files were provided')
        ssl_context = None
    host = 'api.development.push.apple.com' if sandbox else 'api.push.apple.com'
    connection = HTTP20Connection(host=host, port=port, secure=True, ssl_context=ssl_context)

    def send_to_apns(payload, push_id, apns_id=None, topic=topic, expiration=None, has_priority=True):
        url = '/3/device/%s' % push_id

        if isinstance(expiration, datetime):
            expiration = int(expiration.timestamp())

        priority = '10' if has_priority else '5'

        headers = {}
        _add(headers, 'apns-id', apns_id)
        _add(headers, 'apns-topic', topic)
        _add(headers, 'apns-expiration', expiration)
        _add(headers, 'apns-priority', priority)

        stream_id = connection.request(
            method='POST', url=url, body=json.dumps(payload, cls=encoder_cls), headers=headers)
        response = connection.get_response(stream_id=stream_id)
        with response:
            return response.status == 200, response

    return send_to_apns
