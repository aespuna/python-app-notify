python-app-notify
=================

This is a package that provides notifiers and utils for both GCM and APNS


WHY?
====

Have a very simple and single interface to send push notifications to mobile phones.


HOW TO
======

It is very easy to use. You just have to create a notifier::

    notifer = android_notifier(api_key=config.GCM_API_KEY)

or::

    notifier = ios_notifier(config.APNS_CERT_FILE, config.APNS_KEY_FILE, topic='com.example.your_topic')

And then you can use it with::

    was_ok, _ = notifier({'key': 'value'}, push_id)

You can choose which notifier to use based on the ``push_id`` value.

Due to its synchronized nature it is recommended to use it out of your request cycle (using celery or even spawning a new thread is probably a better idea)
