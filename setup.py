from setuptools import find_packages, setup

setup(
    name='python-app-notify',
    version='1.0.0',
    description='Send notifications to mobile apps using GCM or APNS',
    packages=find_packages(),
    install_requires=['hyper', 'requests'],
)
